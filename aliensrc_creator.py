#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
# SPDX-FileCopyrightText: 2021 Alberto Pianon <pianon@array.eu>

import os
import re
import sys
import glob
import json
import hashlib
import argparse
import tempfile
import subprocess
from multiprocessing import Pool as MultiProcessingPool
from urllib.parse import urlparse

SUPPORTED_ARCHIVES = {
    ".gz": {
        "tarparam": "z"
    },
    ".tgz": {
        "tarparam": "z"
    },
    ".xz": {
        "tarparam": "J"
    },
    ".bz2": {
        "tarparam": "j"
    },
    ".lz": {
        "tarparam": "--lzip -"
    },
    ".crate": {
        "tarparam": "z"
    }
}

def sha1sum(filename):
    sha1 = hashlib.sha1()
    with open(filename, 'rb') as f:
        while True:
            block = f.read(65536)
            if not block:
                break
            sha1.update(block)
    return sha1.hexdigest()

def get_dir_sha1(dirpath, excludes=[]):
    opts = ""
    for exclude in excludes:
        opts += f" -not -path '{exclude}'"
    o, _ = bash(f"find . -type f{opts}" + " -exec sha1sum '{}' \; | sed -E 's#  \./#  #'", cwd=dirpath)
    return { l.split(maxsplit=1)[1]: l.split(maxsplit=1)[0] for l in o.split('\n') if l }


class BashException(Exception):
    pass

def bash(command, cwd=None):
    """Run a command in bash shell, and return stdout and stderr
    :param command: the command to run
    :param cwd: directory where to run the command (defaults to current dir)
    :return: stdout and stderr of the command
    :raises: BashException if command exit code != 0
    """
    out = subprocess.run(
        command,
        shell=True,
        executable="/bin/bash",
        cwd=cwd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout = out.stdout.decode()
    stderr = out.stderr.decode()
    if out.returncode != 0:
        raise BashException(
            f"Command '{command}' failed. Output is:\n" f"{stdout}\n{stderr}"
        )
    return stdout, stderr

class AlienSrcException(Exception):
    pass

class AlienSrcCreator:

    def __init__(
        self,
        bom_file: str,
        target_dir: str = os.curdir,
        tmp_dir: str = None
    ):
        with open(bom_file) as f:
            self.bom = json.load(f)
        self.target_dir = target_dir
        self.tmp_dir = tmp_dir

    def process(self):
        for recipe_id in self.bom:
            self.create_aliensrc_pkg(recipe_id)

    def create_aliensrc_pkg(self, recipe_id):
        tarfile = os.path.join(
            os.path.abspath(self.target_dir),
            f'{recipe_id}.aliensrc'
        )
        if SKIP_EXISTING and os.path.isfile(tarfile):
            print(f"{os.path.basename(tarfile)} already exists, skipping")
            return
        recipe_item = self.bom[recipe_id]
        print(
            f"started aliensrc package creation for recipe '{recipe_id}'\n"
        )
        metadata = {
            k: v
            for k, v in recipe_item['recipe']['metadata'].items()
            if k != 'depends_provides'
            # depends_provides may vary depending on the
            # target_machine, so we leave that information only in
            # tinfoilhat.json files
        }
        source_files = recipe_item['recipe']['source_files']
        name = []
        if metadata['base_name'] != metadata['name']:
            name.append(metadata['base_name'])
        name.append(metadata['name'])
        version = f"{metadata['version']}-{metadata['revision']}"

        for f in source_files:
            if not os.path.isabs(f['relpath']):
                f["fullpath"] = os.path.join(f['rootpath'], f['relpath'])
            else:
                f["fullpath"] = os.path.realpath(f['relpath'])
                f["rootpath"], f["relpath"] = os.path.split(f["fullpath"])
            url = urlparse(f['src_uri'].split(';')[0])
            if url.scheme == 'git' or url.scheme == 'gitsm':
                f["git_tmpdir_obj"] = tempfile.TemporaryDirectory(
                    dir=self.tmp_dir
                )
                git_tmpdir = f["git_tmpdir_obj"].name
                if f['relpath'].endswith(".tar.gz"):
                    git_archive = os.path.join(f['rootpath'], f['relpath'])
                    archive_tmpdir_obj = tempfile.TemporaryDirectory(
                        dir=self.tmp_dir
                    )
                    archive_tmpdir = archive_tmpdir_obj.name
                    bash(f"tar -xzf {git_archive}", cwd=archive_tmpdir)
                    if f['relpath'].startswith('gitshallow_'):
                        bare_repo_dir = os.path.join(archive_tmpdir, '.git')
                    else:
                        bare_repo_dir = archive_tmpdir
                    n = re.sub('^(git2_|gitshallow_)', '', f['relpath'])
                    n = re.sub('\.tar\.gz$', '', n)
                    n = re.sub('_[0-9a-f]{7}-', "/", n)
                    repo_dotted_url = n.split("/")[0]
                else:
                    bare_repo_dir = os.path.join(f['rootpath'], f['relpath'])
                    repo_dotted_url = f['relpath']
                repo_name = os.path.splitext(os.path.basename(url.path))[0]
                repo_name = repo_name or 'git'
                repo_dir = os.path.join(git_tmpdir, repo_name)
                git_dir = os.path.join(repo_dir, '.git')
                sha1 = f["git_sha1"]
                os.makedirs(git_dir)
                bash(f'rsync -rLptgoD {bare_repo_dir}/ {git_dir}')
                bash(f'git config --local --bool core.bare false', cwd=repo_dir)
                bash(f'git reset --hard', cwd=repo_dir)
                if url.scheme == 'gitsm':
                    print(
                        'WARNING: use of "gitsm" fetcher is really BAD for'
                        ' source code packaging and analysis,'
                        ' please switch to "git" fetcher ASAP!'
                        ' We try to create the alien package anyway,'
                        ' but metadata on sources will NOT be accurate'
                        ' (see Yocto Manual sec. 44.3.6, "Notes and Warnings").'
                    )
                    bash(f'git checkout {sha1}', cwd=repo_dir)
                    bash(
                        'git submodule update --init --recursive',
                        cwd=repo_dir
                    )
                if "/" not in repo_dotted_url:
                    repo_name_version = f'{repo_dotted_url}@{sha1[:8]}'
                else:
                    repo_name_version = f'{repo_name}@{sha1[:8]}'
                bash(f'mv {repo_name} {repo_name_version}', cwd=git_tmpdir)
                repo_dir = os.path.join(git_tmpdir, repo_name_version)
                tar_name = f'{repo_name_version}.tar.xz'
                tar_path = os.path.join(git_tmpdir, tar_name)
                if url.scheme == 'git':
                    bash('git config tar.tar.xz.command "xz -c"', cwd=repo_dir)
                    bash(
                        f'git archive --format=tar.xz {sha1} > {tar_path}',
                        cwd=repo_dir
                    )
                    # use git archive command for deterministic output
                elif url.scheme == 'gitsm':
                    bash(f'rm -Rf .git', cwd=repo_dir)
                    bash(f'tar -cJf {tar_path} .',cwd=repo_dir)
                f['sha1_cksum'] = sha1sum(tar_path)
                f['rootpath'] = git_tmpdir
                f['relpath'] = tar_name
            elif url.path.endswith(".tar.lz"):
                # workaround for Fossology not supporting lzip archives
                lz_tmpdir_obj = tempfile.TemporaryDirectory(
                    dir=self.tmp_dir
                )
                lz_tmpdir = lz_tmpdir_obj.name
                lz_archive = os.path.join(f['rootpath'], f['relpath'])
                basename_tar_lz = os.path.basename(lz_archive)
                basename_tar, _ = os.path.splitext(basename_tar_lz)
                bash(f"cp {lz_archive} {lz_tmpdir}")
                bash(f"lzip -d {basename_tar_lz}", cwd=lz_tmpdir)
                bash(f"gzip {basename_tar}", cwd=lz_tmpdir)
                f['rootpath'] = lz_tmpdir
                f['relpath'] = f"{basename_tar}.gz"
                f['sha1_cksum'] = sha1sum(os.path.join(f['rootpath'], f['relpath']))
            archive_path = tar_path if url.scheme == 'git' else f["fullpath"]
            _, ext = os.path.splitext(archive_path)
            if ext in SUPPORTED_ARCHIVES:
                tarparam = SUPPORTED_ARCHIVES[ext]["tarparam"]
                stdout, _ = bash(f"tar {tarparam}tf {archive_path}")
                files_in_archive = [
                    f for f in stdout.split("\n")
                    if f and not f.endswith("/")
                ]
                f['files_in_archive'] = len(files_in_archive)
            else:
                f['files_in_archive'] = False

        files = []
        for f in source_files:
            if not os.path.isdir(f["fullpath"]) or f["git_sha1"]:
                files.append({
                    'name': f['relpath'],
                    'git_sha1': f['git_sha1'],
                    'sha1_cksum': f['sha1_cksum'],
                    'src_uri': f['src_uri'],
                    'files_in_archive': f['files_in_archive'] ,
                    'paths': [ os.path.join('_tagged', tag) for tag in f['tags'] ]
                })
            else:
                files_in_dir = get_dir_sha1(f["fullpath"])
                for fpath, sha1 in files_in_dir.items():
                    files.append({
                        'name': os.path.join(f["relpath"], fpath),
                        'git_sha1': f['git_sha1'],
                        'sha1_cksum': sha1,
                        'src_uri': f['src_uri'],
                        'files_in_archive': f['files_in_archive'] ,
                        'paths': [ os.path.join('_tagged', tag) for tag in f['tags'] ]
                    })
        aliensrc_json = {
            'version': 1,
            'source_package': {
                'name': name,
                'version': version,
                'manager': 'bitbake',
                'metadata': metadata,
                'files': files,
                'tags': recipe_item['tags']
            }
        }
        tmpdir_obj = tempfile.TemporaryDirectory(dir=self.tmp_dir)
        tmpdir = tmpdir_obj.name
        with open(os.path.join(tmpdir, 'aliensrc.json'), 'w') as j:
            json.dump(aliensrc_json, j, indent=4)
        tmpdir_files = os.path.join(tmpdir, 'files')
        os.mkdir(tmpdir_files)
        for f in source_files:
            if os.path.isfile(f["fullpath"]) or os.path.isdir(f["fullpath"]):
                paths = [ os.path.join('_tagged', tag) for tag in f['tags'] ] or [""]
                for path in paths:
                    target_path = os.path.join(tmpdir_files, path)
                    os.makedirs(target_path, exist_ok=True)
                    bash(
                        f'rsync -rLptgoR {f["relpath"]} {target_path}',cwd=f["rootpath"]
                    )
                if f.get("git_tmpdir_obj"):
                    f["git_tmpdir_obj"].cleanup()
            else:
                raise AlienSrcException(f"can't find file {fullpath}")
        bash(f'tar -cf {tarfile} aliensrc.json', cwd=tmpdir)
        # metadata go first, so it's quickier to extract them from archive
        bash(f'tar -rf {tarfile} files/', cwd=tmpdir)
        tmpdir_obj.cleanup()
        print(
            f"ended aliensrc package creation for recipe '{recipe_id}'"
        )

    @staticmethod
    def execute(tfh_file):
        asc = AlienSrcCreator(tfh_file, OUTPUT_DIR, TMP_DIR)
        asc.process()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(conflict_handler='resolve')

    parser.add_argument(
        "--tmpdir",
        type = str,
        default = None,
        help = "temporary dir to use for package creation"
    )
    parser.add_argument(
        "--skip-existing",
        action = "store_true",
        default = False,
        help = "skip already existing packages"
    )
    parser.add_argument(
        "OUTPUT_DIR",
        help = "dir where to save aliensrc packages"
    )
    parser.add_argument(
        "TINFOILHAT_DIR",
        nargs = "*",
        help = ("dir(s) where to look for .tinfoilhat.json files"
            " (wildcards allowed)")
    )
    try:
        args = parser.parse_args()
        tfh_dirs = []
        for d in args.TINFOILHAT_DIR:
            tfh_dirs += glob.glob(d)
        for tfh_dir in tfh_dirs:
            tfh_files = glob.glob(os.path.join(tfh_dir, '*.tinfoilhat.json'))
            global OUTPUT_DIR
            global TMP_DIR
            global SKIP_EXISTING
            OUTPUT_DIR = args.OUTPUT_DIR
            TMP_DIR = args.tmpdir
            SKIP_EXISTING = args.skip_existing
            multiprocessing_pool = MultiProcessingPool()
            multiprocessing_pool.map(AlienSrcCreator.execute, tfh_files)

    except Exception as e:
        print(f"\nGot error {e.__class__.__name__}: {e}\n")
        sys.exit(1)
