---
SPDX-License-Identifier: CC-BY-SA-2.0-UK
SPDX-FileCopyrightText: 2021 Alberto Pianon <pianon@array.eu>
---

## Some notes about Tinfoil (not Hat)

Bitbake recipes contain a lot of metadata (in the form of environment variables) that may be useful for SCA. However, one cannot just parse recipe files to get those metadata, because environment variables are usually dynamically modified or overwritten at build time, depending on includes, overrides, etc.

Tinfoil is a [wrapper around BitBake's internal code](https://wiki.yoctoproject.org/wiki/TipsAndTricks/Tinfoil) that can be used to get the value that environment variables actually assume at build time.

It must be run *within* a bitbake build dir *after* a build has been successfully run, to get metadata about *that specific* build.

Tinfoil runs a bitbake server and makes calls to it; so the `prepare()` method must be called before using it, and the `shutdown()` method after using it (or one has to use it within a `with` statement).

Tinfoil is a bitbake module but it is *not intended to be installed* on the system: to import it, one should set the PYTHONPATH env variable to the local bitbake `lib/` dir. To use it, one should `chdir` to a yocto build dir that contains `conf/local.conf` and `conf/bblayers.conf` dirs.

Raw usage example:

```python
import os
from bb.tinfoil import Tinfoil

os.chdir('/build/yocto/build-linux-qemux86-64')
with Tinfoil() as tf:
  tf.prepare()
  tmpdir = tf.config_data.getVar('TMPDIR')
  recipe = tf.parse_recipe('zlib')
  zlib_version = recipe.getVar('PV')
  zlib_sources = recipe.getVar('S')
  print(f"TMPDIR is {tmpdir}")
  print(f"zlib version is {zlib_version}")
  print(f"zlib compiled sources can be found at {zlib_sources}")
```
