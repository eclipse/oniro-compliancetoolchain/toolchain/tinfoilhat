---
SPDX-License-Identifier: CC-BY-SA-2.0-UK
SPDX-FileCopyrightText: 2021 Alberto Pianon <pianon@array.eu>
---

# Bitbake recipe variants vs. Software Composition Analysis

To do Software Composition Analysis (for IP compliance + CVE management), we need source packages to inspect, uniquely identified by name/version, and we need to keep track of changes between different versions of the same source package, in order to audit only things that have changed, and automatically reuse our clearing work for things that did not change.

## 1. The problem: recipe version and revision are not unique identifiers

### 1.1. Possible layer overlays

The problem is that Bitbake does not provide source packages uniquely identified by name/version, but uses layers of recipes (identified by name/version/revision) whose variables can be always overridden by other layers. This may lead to **recipes that are *apparently* identified by the same name/version/revision but that include *different* sets of source files** (typically, different sets of patches applied to the same upstream source archive), or even different versions of the same source file (typically, different versions of a configuration file or of an init script).

Moreover, since new layers may be added to a project over time, we could even expect to have (apparently) the same recipe/version/revision, built for the same target machine, that actually “uses” different sets of source files across different releases of the same project (because f.e. a new layer has been added in a newer release, and that new layer prepends some different stuff to FILESPATH for that recipe, causing the same file://foo.conf src_uri pointing to a different file).

### 1.2. No strict versioning policy

Finally, the Yocto Project does not have a policy to reflect recipe file changes by updating recipe version or revision; thus it may well happen that, in the upstream meta-layer git repo, some source files are added to a recipe over time (typically, new CVE patches) but version and revision stay the same. So, again, we may have the same recipe name, version and revision, that includes different sets of source files over time.

### 1.3. AllScenarios OS

Even if all the above is true for any yocto-based project, it particularly affects such a peculiar project like AllScenarios OS: AllScenarios OS does not consist of a single yocto project, or of a bunch of independent yocto projects that may occasionally share some components, but it's a single project with a significant number of variants and subvariants (distros, target machines and images etc.) so the need to handle such diversity and related complexity in an efficient and consistent way is key.

## 2. The solution: variant groups + recipe and source file tags

While “converting” bitbake recipes in source packages, we would like to avoid to generate a different source package archive for each possible output variant of the same recipe, because it would cause a lot of overhead both in the machine processing and – what is more important – in the human audit process.

So, as long as the upstream source is the same, we would like to create a single source package out of a certain bb recipe, that includes all possible variants of patches, init scripts, configuration files etc. but we would like to tag them appropriately, in order to understand when and where they are used. This will be important not only for IP checks but also for CVE checks (eg. a patch file may fix a CVE, it’s important to know when and where it’s actually applied).

### 2.1. Some examples: variants within the same project release, depending on the machine target

Recipe `busybox-1.31.1-r0` contains `busybox-1.31.1.tar.bz2` as “common” upstream source tarball, plus a series of patches, config files and init scripts (let’s call all of them “Downstream Patches”). In a certain project release, there are 48 “common” Downstream Patches, that are always applied, and 5 Downstream Patches that are used only when building for a specific target machine. We want to avoid generating two different source packages from the same recipe `busybox-1.31.1-r0` (thus avoiding having 2 different source uploads to fossology for the same recipe), so we need to appropriately tag those 5 Downstream Patches in our “alien” source package.

An extreme example is `xserver-xf86-config`, where you have a different `xorg.conf` source file for every different group of target machines, so you would need to create a different source package for each target machine; even if it’s not a recipe that contains actual source code, it’s an example of what could happen given how bitbake recipes work.

### 2.2. Some examples: variants across different releases

Let's say that in a certain project release recipe `curl-7.69.1-r0` includes `curl-7.69.1.tar.bz2` as upstream source tarball, an 7 Downstream Patches, applied to all target machine builds.

The very same recipe `curl-7.69.1-r0` (same version, same revision) in the next release of the same project contains 9 Downstream Patches, and the new 2 ones (`CVE-2021-22876.patch` and `CVE-2021-22890.patch`) are clearly new vulnerability fixes. But recipe version and revision are the same!

With a package manager this would never happen, and generally it should never happen even without a package manager. But here it does happen, and it’s clearly an upstream issue in Yocto project's recipe versioning policy.

So in this case we need to add a "variant" tag (*i.e* a unique hash that identifies actually included source files) to recipe version/revision.

## 3. The solution, in practice

### 3.1. Tag lists

To describe variants across the same project release, we use a tag lists for the whole recipe and possible tag lists for single source files that are used only for certain targets.

Each tag is structured as a path, in the following way:

`<project>/<release>/<distro>/<machine>/<image>`

Recipe tag lists tell in which project releases, distros and target machines/images that particular recipe is used. Eg.:

```
ohos/v0.1.0/openharmony-linux/qemuarm64/openharmony-image-extra
ohos/v0.1.0/openharmony-linux/qemuarm64/openharmony-image-extra-tests
ohos/v0.1.0/openharmony-linux/qemux86-64/openharmony-image-extra
ohos/v0.1.0/openharmony-linux/qemux86-64/openharmony-image-extra-tests
```

Source file tag lists, if any, signal that a particular source file is not used in all distros/targets included in the recipe's tag list, but only in a subset of them. Eg.:

```
ohos/v0.1.0/openharmony-linux/qemuarm64
```

### 3.2. Variant hash

To uniquely identify variants across different project releases (or across different projects), we calculate a checksum of the sources actually included in each recipe variant.

Since sources may be git repos, downloaded tarballs, or local files, we use a different hash to identify each category: git commit sha1, if it's a git repo, and sha1 checksum if it's a downloaded archive or a local file. Then we sort all hashes, concatenate them in a single string, and calculate the sha1 hash of that string.

Finally, for convenience, we use the first 8 characters of the resulting sha1 hash as unique "variant" tag for the recipe.
