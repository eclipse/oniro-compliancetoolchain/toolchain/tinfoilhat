---
SPDX-License-Identifier: CC-BY-SA-2.0-UK
SPDX-FileCopyrightText: 2021 Alberto Pianon <pianon@array.eu>
SPDX-FileCopyrightText: 2010-2020 Linux Foundation
date: 2020-03-14
---

# 1. Bitbake environment variables relevant for SCA

This is a subset of bitbake's environment variables that may be relevant for Software Composition Analysis / IP Compliance purposes.

**Excerpt and adaptation from the Glossary of the Yocto Mega Manual Revision 3.1.**

*Additional notes by Alberto Pianon.*

> TODO: add SDK-related variables

## 1.1. Build and Distribution

### 1.1.1. Build metadata found in config_data

> NOTE: vars available in tinfoil's `config_data` attr, through `getVar` method, like in `tf.config_data.getVar('TMPDIR')`

TMPDIR
:   The base directory where the OpenEmbedded build system performs all its work during the build. The default base directory is the tmp directory.

DEPLOY_DIR
:   Points to the general area that the OpenEmbedded build system uses to place images, packages, SDKs, and other output files that are ready to be used outside of the build system. By default, this directory resides within the Build Directory as `${TMPDIR}/deploy`.

DEPLOY_DIR_IMAGE
:   Points to the area that the OpenEmbedded build system uses to place images and other associated output files that are ready to be deployed onto the target machine. The directory is machine-specific as it contains the `${MACHINE}` name. By default, this directory resides within the Build Directory as `${DEPLOY_DIR}/images/${MACHINE}/`.

> NOTE: In the glossary there is also an IMAGE_MANIFEST var, which would be useful if only Tinfoil managed to get it, but it doesn't...

PKGDATA_DIR
:   Points to a shared, global-state directory that holds data generated during the packaging process. For examples of how this data is used, see **the "Viewing Package Information with oe-pkgdata-util" section** in the Yocto Project Development Tasks Manual.

> NOTE: It is useful to look at `oe-pkgdata-util` code at `<yocto_dir>/sources/poky/scripts/oe-pkgdata-util`

### 1.1.2 Recipe data

> NOTE: vars available in tinfoil-parsed recipes, like in: `recipe = tf.parse_recipe('zlib'); WORKDIR = recipe.getVar('WORKDIR')`

PACKAGE_ARCH
:   The architecture of the built package or packages

WORKDIR
:   The location where the OpenEmbedded build system builds a recipe

S
:   The location in the Build Directory where unpacked recipe source code resides. By default, this directory is `${WORKDIR}/${BPN}-${PV}`

> NOTE: The source code witin `S` is already patched, configured and built, so it is not the original upstream source code.

## 1.2 Package metadata

> NOTE: vars available in tinfoil-parsed recipes, like in: `recipe = tf.parse_recipe('zlib'); PN = recipe.getVar('PN')`

PN
:   The name of the recipe used to build the package. This variable can have multiple meanings. However, when used in the context of input files, PN represents the the name of the recipe.

BPN
:   This variable is a version of the `PN` variable with common prefixes and suffixes removed, such as `nativesdk-`, `-cross`, `-native`, and multilib's `lib64-` and `lib32-`.

> NOTE: In order to identify the upstream source package, it is better to use `BPN`.

PV
:   The version of the recipe. The version is normally extracted from the recipe filename. `PV` is generally not overridden within a recipe **unless it is building an unstable (i.e. development) version** from a source code repository (e.g. Git or Subversion).

> NOTE: Generally, `PV` should be used to collect source package version. For packages built from git repos, `PV` looks like `1.2.0+gitAUTOINC+4a062cf418` where `1.2.0` is the latest version tag, `gitAUTOINC` means that recipe does not point to a specific revision but to the latest commit available, and `4a062cf418` is the commit's short sha1 hash.
> There are also other environment variables where to fetch single pieces of version information (`AUTOREV`, `SRCREV`, `SRCPV` etc.) but since they do not "behave" exactly as described in the manual (and may even contain misleading values, like `INVALID`), and since all info can be collected by parsing `PV`, it is easier to use just `PV`.
> There is a problem, though: with

PR
:   The revision of the recipe used to build the package. The default value for this variable is "r0". **The OpenEmbedded build system does not need the aid of PR** to know when to rebuild a recipe. The build system uses the task input checksums along with the stamp and shared state cache mechanisms. **The PR variable primarily becomes significant when a package manager dynamically installs packages on an already built image**.

> NOTE: this means that `PR` is  generally irrelevant unless there are pakages dynamically installed on already built images.

EXTENDPKGV
:   The full package version specification as it appears on the final packages produced by a recipe.

> NOTE: basically, it is `$PV-$PR`

## 1.3. Sources

> NOTE: vars available in tinfoil-parsed recipes, like in: `recipe = tf.parse_recipe('zlib'); FILESPATH = recipe.getVar('FILESPATH')`

FILESPATH
:   The default set of directories the OpenEmbedded build system uses when searching for patches and files. During the build process, BitBake searches each directory in `FILESPATH` in the specified order when looking for files and patches specified by each `file:// URI` in a recipe's `SRC_URI` statements.

FILESEXTRAPATHS
:   Extends the search path the OpenEmbedded build system uses when looking for files and patches as it processes recipes and append files. The default directories BitBake uses when it processes recipes are initially defined by the `FILESPATH` variable. You can extend `FILESPATH` variable by using `FILESEXTRAPATHS`.

SRC_URI
:   The list of source files - local or remote. This variable tells the OpenEmbedded build system which bits to pull in for the build and how to pull them in. available URI protocols: `file://` (The path is relative to the `FILESPATH` variable);  `git://` `repo://` `svn://` `bzr://` `osc://` `ccrc://` `cvs://` `hg://` `p4://` (revision control repositories); `http://` `https://` `ftp://` `ssh://`, `npm://` (JavaScript modules). Not all patch files are always applied, it depends on the options.[^patch] **The name of the downloaded file can be manually set through `downloadfilename` option**

  [^patch]: **Standard and recipe-specific options for SRC_URI exist**. The relevant ones from a SCA/IP Compliance perspective, are: **apply** (Whether to apply the patch or not. The default action is to apply the patch) and **mindate/maxdate/minrev/maxrev/rev/notrev** (i.e. apply the patch only if `SRCDATE|SRCREV` is (equal to)|(lower|greater than) date|rev), because they decide if a patch is actually used or not in build.

> NOTE: SRC_URI is a string with list items separated by spaces, when parsed by Tinfoil

DL_DIR
:   The central download directory used by the build process to store downloads. By default, `DL_DIR` gets files suitable for mirroring for everything except Git repositories.

> NOTE: downloaded files/tarballs should retain the same filename they have in `SRC_URI`, unless overridden by downloadfilename option. Options are separated by `;` at the end of the uri.

GITDIR
:   The directory in which a local copy of a Git repository is stored when it is cloned.

> NOTE: unfortunately, `GITDIR` var does not seem to "work" properly in Tinfoil, as it is empty also in recipes that do use git repos to download sources. BTW git clones may be found in `$DL_DIR/git2/<url>` where `<url>` is git remote url with slashes (`/`) replaced with dots (`.`).

## 1.4. Dependencies

> NOTE: vars available in tinfoil-parsed recipes, like in: `recipe = tf.parse_recipe('zlib'); DEPENDS = recipe.getVar('DEPENDS')`

DEPENDS
:   Lists a recipe's build-time dependencies. These are dependencies on other recipes whose contents (e.g. headers and shared libraries) are needed by the recipe at build time.

PROVIDES
:   A list of aliases by which a particular recipe can be known. By default, a recipe's own PN is implicitly already in its `PROVIDES` list and therefore does not need to mention that it provides itself. If a recipe uses `PROVIDES`, the additional aliases are synonyms for the recipe and can be useful for satisfying dependencies of other recipes during the build as specified by `DEPENDS`.

## 1.5. LICENSE

### 1.5.1. package/recipe

> NOTE: var available in tinfoil-parsed recipes, like in: `recipe = tf.parse_recipe('zlib'); LICENSE = recipe.getVar('LICENSE')`

LICENSE
:   The list of source licenses for the recipe. Follow these rules: no spaces in license names, `|` (dual license), `&` (**multiple licenses that cover parts of the source**). For standard licenses, use the names of the files in **meta/files/common-licenses/** or the **`SPDXLICENSEMAP` flag names defined in meta/conf/licenses.conf**. You can also specify **licenses on a per-package basis** to handle situations where components of the output have different licenses. For example, a piece of software whose code is licensed under GPLv2 but has accompanying documentation licensed under the GNU Free Documentation License 1.2 could be specified as follows:

```
 LICENSE = "GFDL-1.2 & GPLv2"
 LICENSE_${PN} = "GPLv2"
 LICENSE_${PN}-doc = "GFDL-1.2"
```

> NOTE: unfortunately, licenses identifiers are not in SPDX format. There is a `**SPDXLICENSEMAP`, but unfortunately it is highly incomplete and outdated

### 1.5.1. Relevant config_data

> NOTE: vars available in tinfoil's `config_data` attr, through `getVar` method, like in `tf.config_data.getVar('LICENSE_PATH')`

COMMON_LICENSE_DIR
:   Points to `meta/files/common-license`s in the Source Directory, which is where generic license files reside.

LICENSE_PATH
:   Path to additional licenses used during the build. By default, the OpenEmbedded build system uses `COMMON_LICENSE_DIR` to define the directory that holds common license text used during the build.

AVAILABLE_LICENSES
:   List of licenses found in the directories specified by `COMMON_LICENSE_DIR` and `LICENSE_PATH`.

SPDXLICENSEMAP
:   Maps commonly used license names to their SPDX counterparts found in `meta/files/common-licenses/`. For the default SPDXLICENSEMAP mappings, see the `meta/conf/licenses.conf` file.

> NOTE: SPDXLICENSEMAP does not seem to be loaded by Tinfoil. BTW, the map can be found in `sources/poky/meta/conf/licenses.conf`, but it is incomplete and outdated (it contains many deprecated SPDX license identifiers)

## 1.6. Other Metadata

> NOTE: var available in tinfoil-parsed recipes, like in: `recipe = tf.parse_recipe('zlib'); AUTHOR = recipe.getVar('AUTHOR')`

AUTHOR
:   The email address used to contact the original author or authors in order to send patches and forward bugs.

HOMEPAGE
:   Website where more information about the software the recipe is building can be found.

SUMMARY
:   The short (72 characters or less) summary of the binary package for packaging systems such as opkg, rpm, or dpkg.

DESCRIPTION
:   The package description used by package managers. If not set, DESCRIPTION takes the value of the `SUMMARY` variable.
