---
SPDX-License-Identifier: CC-BY-SA-2.0-UK
SPDX-FileCopyrightText: 2021 Alberto Pianon <pianon@array.eu>
date: 2020-03-14
---

# Note on bitbake archiver class

To do SCA on bitbake-based projects, one needs to get source packages (including full corresponding sources and relevant metadata for each recipe and package).

To that purpose, in theory the bitbake archiver class described [here](https://www.yoctoproject.org/docs/current/mega-manual/mega-manual.html#providing-the-source-code) and [here](https://www.yoctoproject.org/docs/current/mega-manual/mega-manual.html#var-ARCHIVER_MODE) looks promising, because after build you should get corresponding sources of all packages contained in the final image into a folder `tmp/deploy/sources/<arch>/<package>` (even in srpm format, if you wish).

The problem is that it seems that for quite a number of packages bitbake silently fails to deploy sources: folder `tmp/deploy/sources/<arch>/<package>` remains empty, and also within recipe workdir, the folder `deploy-sources/<arch>/<package>` is empty, and the folder `deploy-sources-rpm/<arch>/<package>` contains an srpm file, but if you look into it, there are no actual sources, just metadata. You get no warning and no other useful info in the recipe workdir logs. But sources are actually present in the recipe workdir, as well as the built binaries, so something is wrong with the archiver.

As of 2021-03-12, this bug has not been reported in [bugzilla](https://bugzilla.yoctoproject.org/buglist.cgi?quicksearch=archiver)

> TODO: file bug report

<!-- internal note

tried with build-ohos-linux-qemux86, using the following params in `conf/local.conf`:

```
INHERIT += "archiver"
ARCHIVER_MODE[src] = "original"
ARCHIVER_MODE[dumpdata] = "1"
ARCHIVER_MODE[recipe] = "1"
ARCHIVER_MODE[srpm] = "1"
```

Problem encountered with package/recipe `cantarell-fonts` and many many others
-->
