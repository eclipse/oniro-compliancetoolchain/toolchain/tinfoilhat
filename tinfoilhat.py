#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
# SPDX-FileCopyrightText: 2021-2024 Alberto Pianon <pianon@array.eu>

import os
import re
import sys
import csv
import glob
import yaml
import json
import hashlib
import argparse
import tempfile
import subprocess
from typing import Set, Dict, List
from dataclasses import dataclass, field, replace

_lastline = ""

def _stderr_rewrite_line(text):
    global _lastline
    sys.stderr.write("\r" + " " * len(_lastline))
    sys.stderr.write("\r" + text)
    _lastline = text

def bash(command, cwd=None):
    """Run a command in bash shell, and return stdout and stderr
    :param command: the command to run
    :param cwd: directory where to run the command (defaults to current dir)
    :return: stdout and stderr of the command
    :raises: BashException if command exit code != 0
    """
    out = subprocess.run(
        command,
        shell=True,
        executable="/bin/bash",
        cwd=cwd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout = out.stdout.decode()
    stderr = out.stderr.decode()
    if out.returncode != 0:
        raise TinfoilHatException(
            f"Command '{command}' failed. Output is:\n" f"{stdout}\n{stderr}"
        )
    return stdout, stderr

def is_sha1(string):
    if len(string) != 40:
        return False
    try:
        _ = int(string, 16)
        return True
    except ValueError:
        return False

def convert2serialize(obj):
    if isinstance(obj, dict):
        return { k: convert2serialize(v) for k, v in obj.items() }
    elif hasattr(obj, "_ast"):
        return convert2serialize(obj._ast())
    elif not isinstance(obj, str) and hasattr(obj, "__iter__"):
        l = [ convert2serialize(v) for v in obj ]
        try:
            return sorted(l)
        except TypeError: # not sortable
            return l
    elif hasattr(obj, "__dict__"):
        return {
            k: convert2serialize(v)
            for k, v in obj.__dict__.items()
            if not callable(v) and not k.startswith('_')
        }
    else:
        return obj

def find_files(directory):
    res = []
    for root, dirs, files in os.walk(directory):
        d = os.path.relpath(root, directory)
        if d != '.':
            res.append(f'/{d}/')
        for f in files:
            res.append(os.path.join("/", d, f))
    return res

def get_dir_sha256(dirpath, excludes=[]):
    opts = ""
    for exclude in excludes:
        opts += f" -not -path '{exclude}'"
    o, _ = bash(f"find . -type f{opts}" + " -exec sha256sum '{}' \; | sed -E 's#  \./#  #'", cwd=dirpath)
    return { l.split(maxsplit=1)[1]: l.split(maxsplit=1)[0] for l in o.split('\n') if l }

def parse_pkgdata(pkgdata_file):
    pkgdata_text = pkgdata_file.read()
    pkgdata = {}
    p = re.compile('^(.+): (.*)$')
    lines = pkgdata_text.split('\n')
    for line in lines:
        m = p.match(line)
        if m:
            key = m.group(1)
            value = (
                json.loads(m.group(2))
                if key == 'FILES_INFO'
                else m.group(2).strip()
            )
            pkgdata.update({ key: value })
    return pkgdata

def parse_bb_depends_provides(bb_depends_provides_string: str) -> List[str]:
    """Parse a bitbake RDEPENDS or RPROVIDES string that may look like this:
    'base-files update-alternatives-opkg musl (>= 1.2.0+git0+040c1d16b4)
    ncurses-libtinfo (>= 6.2)' and return the list of packages"""
    if not bb_depends_provides_string:
        return [ None, ]
    s = f" {bb_depends_provides_string} "
    pkgs_without_version = re.findall(r'(?<= )([^ ()]+)(?= )(?! \()', s)
    pkgs_with_version = re.findall(r'(?<= )([^ ()]+ \([^\)]+\))(?= )', s)
    return sorted(pkgs_without_version + pkgs_with_version)

def sha1sum(filename):
    sha1 = hashlib.sha1()
    with open(filename, 'rb') as f:
        while True:
            block = f.read(65536)
            if not block:
                break
            sha1.update(block)
    return sha1.hexdigest()

def sha256sum(filename):
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as f:
        while True:
            block = f.read(65536)
            if not block:
                break
            sha256.update(block)
    return sha256.hexdigest()

def sha1sum_str(string):
    return hashlib.sha1(string.encode('utf-8')).hexdigest()

def unique_sorted(input_list: List[str]) -> List[str]:
    """return the same list with unique and sorted values"""
    return sorted(list(set(input_list)))

def aggregate_paths(path_list: List[str]) -> Dict[str, List[str]]:
    """create a dictionary out of a path list, with dirname as key and a list
    of basenames as values, to aggregate last elements of all paths"""
    return {
        os.path.dirname(a) : unique_sorted([
            os.path.basename(aa)
            for aa in path_list
            if aa.startswith(os.path.dirname(a))
        ])
        for a in path_list
    }

class Model:

    @property
    def yaml(self):
        return yaml.dump(convert2serialize(self))

    @property
    def json(self):
        return json.dumps(convert2serialize(self), separators=(',', ':'))

    def copy(self):
        return replace(self)


class BoM(dict, Model):
    pass

@dataclass
class CVEProduct(Model):
    product: str
    cvesInRecord: str

@dataclass
class CVEIssue(Model):
    id: str
    summary: str
    scorev2: str
    scorev3: str
    vector: str
    status: str
    link: str

@dataclass
class CVEMetadata(Model):
    products: List[CVEProduct]
    issue: List[CVEIssue]


@dataclass
class BBTags(Model):
    project: Set[str]
    release: Set[str]
    buildtag: Set[str]
    machine: Set[str]
    image: Set[str]


class BBTagPaths(set, Model):

    keys = ['project', 'release', 'buildtag', 'machine', 'image']

    def __init__(self, paths: Set[str] = set()):
        for path in paths:
            if len(path.split("/")) > len(self.keys):
                raise TinfoilHatException(
                    f"'{path}' is not a valid tag path,"
                    f" must have no more than {len(self.keys)} elements"
                )
        super().__init__(paths)
        # TODO: use pathlib.Path class instead of str?

    def copy(self):
        cls = type(self)
        return(cls(super().copy()))

    @classmethod
    def merge(cls, old: 'BBTagPaths', new: 'BBTagPaths'):
        return cls(set(list(old) + list(new)))

    def update(self, new):
        for elem in new:
            self.add(elem)

    @property
    def aggregate_tags(self) -> BBTags:
        res = {key: set() for key in self.keys}
        for i, key in enumerate(self.keys):
            for elem in self:
                try:
                    res[key].add(elem.split("/")[i])
                except IndexError:
                    pass
        d = {k: set(v) for k, v in res.items()}
        return BBTags(**d)

@dataclass
class BBDependsProvides(Model):
    depends: List[str]
    provides: List[str]

@dataclass
class BBLayer(Model):
    layer_path: str
    remote: str = None
    revision: str = None

@dataclass
class BBSrcFile(Model):
    rootpath: str
    relpath: str
    src_uri: str
    sha1_cksum: str
    git_sha1: str
    tags: BBTagPaths
    override_layer: BBLayer

@dataclass
class BBFile(Model):
    path: str
    sha256: str
    size: int

@dataclass
class BBPackageFiles(Model):
    file_dir: str
    files: List[BBFile]

@dataclass
class BBPackageMetadata(Model):
    name: str
    base_name: str
    version: str
    revision: str
    package_arch: str
    build_toolchain: str
    recipe_name: str
    recipe_version: str
    recipe_revision: str
    license: str
    summary: str
    description: str
    depends: List[str]
    provides: List[str]

@dataclass
class BBPackage(Model):
    metadata: BBPackageMetadata
    files: BBPackageFiles
    chk_sum: str = field(init=False)

    def __post_init__(self):
        self.calc_chk_sum()

    def calc_chk_sum(self):
        list2hash = [ f"{f.path}{f.sha256}{f.size}" for f in self.files.files ]
        self.chk_sum = sha1sum_str(''.join(list2hash))

@dataclass
class BBPackageItem(Model):
    package: BBPackage
    tags: BBTagPaths

@dataclass
class BBRecipeMetadata(Model):
    name: str
    base_name: str
    version: str
    revision: str
    variant: str
    build_toolchains: List[str]
    author: str
    homepage: str
    summary: str
    description: str
    license: str
    depends_provides: Dict[str, BBDependsProvides]

@dataclass
class BBRecipe(Model):
    metadata: BBRecipeMetadata
    cve_metadata: CVEMetadata
    source_files: List[BBSrcFile]
    layer: BBLayer
    chk_sum: str = field(init=False)

    def __post_init__(self):
        self.calc_chk_sum()

    def calc_chk_sum(self):
        if not self.source_files:
            # meta-recipe with no source files but only dependencies
            m = self.metadata
            self.chk_sum = sha1sum_str(f'{m.name}{m.version}{m.revision}')
        else:
            sha1list = [(f.git_sha1 or f.sha1_cksum) for f in self.source_files]
            sha1list.sort()
            self.chk_sum = sha1sum_str(''.join(sha1list))
        self.metadata.variant = self.chk_sum[:8]

@dataclass
class BBRecipeItem(Model):
    recipe: BBRecipe
    tags: BBTagPaths
    packages: Dict[str, BBPackageItem] = field(default_factory=dict)

    def clean_source_file_tags(self):
        """delete source file tags if they are the same as the recipe tags, and
        aggregate paths that contain all possible subelements found in recipe
        tags"""
        for source_file in self.recipe.source_files:
            if sorted(source_file.tags) == sorted(self.tags):
                source_file.tags = BBTagPaths()
                continue
            s_tags = source_file.tags.copy()
            r_tags = self.tags.copy()
            while True:
                a_aggr = aggregate_paths(r_tags)
                s_aggr = aggregate_paths(s_tags)
                found = False
                for dirname in s_aggr:
                    if a_aggr[dirname] == s_aggr[dirname]:
                        # i.e. basename lists are equal
                        s_tags = [
                            s_tag for s_tag in s_tags
                            if not s_tag.startswith(f"{dirname}/")
                        ]
                        r_tags = [
                            a_tag for a_tag in r_tags
                            if not a_tag.startswith(f"{dirname}/")
                        ]
                        s_tags.append(dirname)
                        r_tags.append(dirname)
                        found = True
                if not found:
                    break
            source_file.tags = s_tags

    def add_package_item(self, package_item: BBPackageItem):
        m = package_item.package.metadata
        t = package_item.tags.aggregate_tags
        project = list(t.project)[0]
        release = list(t.release)[0]
        buildtag = list(t.buildtag)[0]
        p = re.compile(f"^{project}-")
        buildtag = p.sub("", buildtag)
        machine = list(t.machine)[0]
        package_id = (
            f'{m.name}-{m.version}-{m.revision}'
            f'+{project}_{release}+{buildtag}+{machine}'
        )
        if self.packages.get(package_id):
            if self.packages.get(package_id).package.chk_sum != package_item.package.chk_sum:
                raise TinfoilHatException(
                    f"Checksum mismatch for package {package_id}"
                )
            self.packages[package_id].tags.update(package_item.tags)
        else:
            self.packages[package_id] = package_item

    def update_source_files(self, source_files: List[BBSrcFile]):
        # indexing...
        def gen_key(s):
            return os.path.realpath(
                os.path.join(s.rootpath, s.relpath)
            ) + "@" + (s.git_sha1 or s.sha1_cksum)

        old_files = {gen_key(s): s for s in self.recipe.source_files}
        new_files = {gen_key(s): s for s in source_files}
        for new_id in new_files:
            if new_id in old_files:
                old_files[new_id].tags.update(new_files[new_id].tags)
            else:
                self.recipe.source_files.append(new_files[new_id])
        self.recipe.calc_chk_sum()

    def check_metadata(self, metadata: BBRecipeMetadata):
        attrs2check = [
            'author',
            'homepage',
            'summary',
            'description',
            'license'
        ]
        for attr2check in attrs2check:
            old = getattr(self.recipe.metadata, attr2check)
            new = getattr(metadata, attr2check)
            if new != old:
                if attr2check == 'license':
                    if new in old:
                        self.recipe.metadata.license = old
                        continue
                    elif old in new:
                        metadata.license = new
                        continue
                m = self.recipe.metadata
                raise TinfoilHatException(
                    f"metadata mismatch in recipe"
                    f" {m.name}-{m.version}-{m.revision}, "
                    f" found different values for '{attr2check}':"
                    f" '{old}' and '{new}'"
                )

    def check_cve_metadata(
        self,
        metadata: BBRecipeMetadata,
        new_cve: CVEMetadata
    ):
        old_cve = self.recipe.cve_metadata
        if not old_cve and not new_cve:
            return True
        # due to a bug of Yocto cve-checker, cve results may be incomplete for
        # some builds in case of random connection problems to NIST db.
        # Moreover, issue details may be updated in NIST database in the time
        # between one build and another (it really happened) as a workaround, we
        # merge data coming from different builds based on product and on cve
        # ids
        old_products = { p.product: p for p in old_cve.products }
        new_products = { p.product: p for p in new_cve.products }
        for old_product_name, old_product in old_products.items():
            if old_product.cvesInRecord == "Yes":
                new_products[old_product_name].cvesInRecord == "Yes"
        new_cve.products = [ product for _, product in new_products.items() ]

        old_issues = { i.id: i for i in old_cve.issue }
        new_issues = { i.id: i for i in new_cve.issue }
        for id, issue in old_issues.items():
            new_issues[id] = issue

        new_cve.issues = [ issue for _, issue in new_issues.items() ]

    def update_build_toolchains(self, build_toolchains):
        old_bt = self.recipe.metadata.build_toolchains
        new_bt = build_toolchains
        merged = list(set(old_bt + new_bt))
        self.recipe.metadata.build_toolchains = merged

    def update_depends_provides(self, depends_provides):
        for machine_tag, new_dp in depends_provides.items():
            old_dp = self.recipe.metadata.depends_provides.get(machine_tag)
            if old_dp:
                if  (
                    sorted(old_dp.depends) != sorted(new_dp.depends)
                    or sorted(old_dp.provides) != sorted(new_dp.provides)
                ):
                    raise TinfoilHatException(
                        f"'depends_provides' mismatch in"
                        f" recipe {self.recipe.metadata.name}"
                        f" for machine '{machine_tag}', values are "
                        f" {old_dp.json} and {new_dp.json}"
                    )
            else:
                self.recipe.metadata.depends_provides[machine_tag] = new_dp


class TinfoilHatException(Exception):
    pass


class TinfoilHat:

    def __init__(self, build_dir: str, project: str, release: str, bom: BoM):
        self._curdir = os.path.abspath(os.curdir)
        try:
            os.chdir(build_dir)
        except FileNotFoundError:
            raise TinfoilHatException(f'Build dir {build_dir} does not exist!')
        self.build_dir = build_dir
        self._load_tinfoil()
        self.tf = Tinfoil()
        self.tf.prepare()
        self.bom = bom
        self.project = project
        self.release = release

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        os.chdir(self._curdir)
        if hasattr(self, 'tf'):
            self.tf.shutdown()

    def _load_tinfoil(self):
        if 'Tinfoil' not in vars() and 'Tinfoil' not in globals():
            if not os.environ.get('PYTHONPATH'):
                raise TinfoilHatException(
                    "please set PYTHONPATH env var, pointing to bitbake+oe lib dirs"
                )
            try:
                global URI, Tinfoil, FetchData, Git, read_subpkgdata
                from bb.tinfoil import Tinfoil
                from bb.fetch2 import URI, FetchData
                from bb.fetch2.git import Git
                from oe.packagedata import read_subpkgdata
            except:
                raise TinfoilHatException(
                    "Can't import modules from bitbake: have you set PYTHONPATH"
                    " env var to bitbake+oe lib dir paths?"
                )


    def get_source_files(self, recipe, tags, layer):
        source_files = {}
        search_paths = recipe.getVar('FILESPATH').split(':')
        DL_DIR = self.config_data.getVar('DL_DIR')
        SOURCE_MIRROR_URL= self.config_data.getVar('SOURCE_MIRROR_URL') or ''
        if SOURCE_MIRROR_URL and not SOURCE_MIRROR_URL.startswith('file://'):
            raise TinfoilHatException(
                "SOURCE_MIRROR_URLs with url scheme other than 'file://' "
                f"(like {SOURCE_MIRROR_URL}) are not supported yet, sorry"
            )
        download_paths = [ DL_DIR ]
        if SOURCE_MIRROR_URL:
            download_paths.append(SOURCE_MIRROR_URL.replace('file://', ''))
        for i in range(0, len(download_paths)):
            if not download_paths[i].endswith("/"):
                download_paths[i] += "/"
        src_uris = recipe.getVar('SRC_URI').split()
        version = recipe.getVar('PV')
        shared_sources_recipe = None
        if not src_uris and '/work-shared/' in recipe.getVar('S'):
            shared_sources = recipe.getVar('S')
            for pn, pv in self.all_recipes.items():
                if pv == version:
                    rr = self.tf.parse_recipe(self.mc_str + pn)
                    if (
                        rr.getVar('S') == shared_sources
                        and rr.getVar('SRC_URI')
                    ):
                        src_uris = rr.getVar('SRC_URI').split()
                        shared_sources_recipe = rr
                        break
        for src_uri in src_uris:
            try:
                uri = URI(src_uri.rstrip(';'))
            except:
                raise TinfoilHatException(f"Can't parse URI '{src_uri}'")
            override_layer = None
            if uri.scheme in ['https', 'http', 'crate']: #FIXME: ftp?
                src_uri_key = src_uri
                if uri.scheme == 'crate':
                    parts = uri.path.split('/')
                    if len(parts) < 3:
                        raise TinfoilHatException("Invalid URL {uri}: Must be crate://HOST/NAME/VERSION")
                    version = parts[-1]
                    name = parts[-2]
                    host = os.path.join(uri.hostname, *parts[:-2]).strip('/')
                    if host == 'crates.io':
                        host = 'crates.io/api/v1/crates'
                    params = uri.params
                    uri = URI(f"https://{host}/{name}/{version}/download")
                    uri.params = params
                    uri.params["downloadfilename"] = f"{name}-{version}.crate"
                src_relpath =  (
                    uri.params['downloadfilename']
                    if uri.params.get('downloadfilename')
                    else os.path.basename(uri.path)
                )
                found = False
                for download_path in download_paths:
                    src_rootpath = download_path
                    fullpath = os.path.join(src_rootpath, src_relpath)
                    if os.path.isfile(fullpath):
                        found = True
                        break
                if not found:
                    raise TinfoilHatException(
                        f"can't find {src_uri} in any of {download_paths}, "
                        f"something's wrong with recipe '{recipe.getVar('PN')}'"
                    )
                sha1_cksum = sha1sum(os.path.join(src_rootpath, src_relpath))
                git_sha1 = None
            elif uri.scheme == 'git' or uri.scheme == 'gitsm':
                src_uri_key = src_uri
                if uri.scheme == 'gitsm':
                    sys.stderr.write(
                        '\n\nWARNING: use of "gitsm" fetcher is really BAD for'
                        ' source code packaging and analysis,'
                        ' please switch to "git" fetcher ASAP!'
                        ' We try to collect metadata anyway,'
                        ' but they will NOT be accurate'
                        ' (see Yocto Manual sec. 44.3.6, "Notes and Warnings").'
                        '\n\n'
                    )
                ud = FetchData(src_uri, recipe)
                git_fetcher = Git(recipe)
                git_fetcher.urldata_init(ud, recipe)
                sha1_cksum = None
                if recipe.getVar("BB_GIT_SHALLOW") == '1':
                    if recipe.getVar("BB_GENERATE_MIRROR_TARBALLS") != '1':
                        raise TinfoilHatException(
                            f"recipe '{recipe.getVar('PN')}' has git shallow"
                            " cloning (BB_GIT_SHALLOW) enabled, but"
                            " BB_GENERATE_MIRROR_TARBALLS is not set, so we"
                            f" can't find sources for '{src_uri}' in any"
                            " download folders"
                        )
                    src_relpath = ud.mirrortarballs[0]
                    found = False
                    for download_path in download_paths:
                        src_rootpath = download_path
                        fullpath = os.path.join(src_rootpath, src_relpath)
                        if os.path.isfile(fullpath):
                            found = True
                            break
                    if not found:
                        raise TinfoilHatException(
                            f"can't find {src_uri} in any of {download_paths}, "
                            f"something's wrong with recipe '{recipe.getVar('PN')}'"
                        )

                else:
                    if not os.path.isdir(ud.localpath):
                        if os.path.isfile(ud.fullmirror):
                            bash(f"mkdir -p {os.path.realpath(ud.localpath)}")
                            bash(f"tar -xzf {ud.fullmirror}", cwd=ud.localpath)
                        else:
                            raise TinfoilHatException(
                                f"can't find {src_uri} in any of {download_paths}, "
                                "something's wrong with recipe"
                                f" '{recipe.getVar('PN')}'"
                            )
                    src_rootpath = os.path.dirname(ud.localpath)
                    src_relpath = os.path.basename(ud.localpath)
                if shared_sources_recipe:
                    git_sha1 = shared_sources_recipe.getVar('SRCREV')
                else:
                    git_sha1 = ud.revision
                if (
                    not git_sha1
                    or git_sha1 == 'INVALID'
                ):
                    raise TinfoilHatException(
                        f"can't get a valid sha1 ({git_sha1}) for repo {src_uri}"
                        f" in recipe {recipe.getVar('PN')}"
                    )
            elif uri.scheme == 'file':
                if os.path.isabs(uri.path):
                    parts = src_uri.split(";", maxsplit=1)
                    params = ";"+parts[1] if len(parts) == 2 else ""
                    src_uri_key = "file://" + os.path.basename(uri.path) + params
                else:
                    src_uri_key = src_uri
                git_sha1 = None
                src_relpath = f'{uri.path}' # filename
                if os.path.isabs(src_relpath):
                    if os.path.isfile(src_relpath) or os.path.isdir(src_relpath):
                        src_rootpath = ""
                    else:
                        raise TinfoilHatException(
                            f"can't find {src_uri}, "
                            f"something's wrong with recipe '{recipe.getVar('PN')}'"
                        )
                else:
                    for path in search_paths:
                        fullpath = os.path.join(path, src_relpath)
                        if os.path.isfile(fullpath) or os.path.isdir(fullpath):
                            src_rootpath = path
                            found = True
                            break
                    else:
                        raise TinfoilHatException(
                            f"can't find {src_uri} in {search_paths}, "
                            f"something's wrong with recipe '{recipe.getVar('PN')}'"
                        )
                if os.path.isfile(os.path.join(src_rootpath, src_relpath)):
                    sha1_cksum = sha1sum(os.path.join(src_rootpath, src_relpath))
                else:
                    sha1_cksum = "DIRECTORY" # FIXME workaround - this is currently handled in aliensrc_creator
                if not src_rootpath.startswith(layer.layer_path):
                    # it's an override from another layer, let's find which one
                    found = False
                    for layer_path in self.layers:
                        if src_rootpath.startswith(layer_path):
                            found = True
                            override_layer = self.layers[layer_path]
                            break
                    if not found:
                        override_layer = "UNKNOWN"
            else:
                raise TinfoilHatException(
                    f"can't handle scheme '{uri.scheme}' found in recipe"
                    f" {recipe.getVar('PN')} SRC_URI"
                )
            # TODO: handle also other schemes supported by bb recipes
            source_files.update({
                src_uri_key: BBSrcFile(
                    src_rootpath,
                    src_relpath,
                    src_uri,
                    sha1_cksum,
                    git_sha1,
                    tags.copy(),
                    override_layer)
            })
        return [ v for k,v in source_files.items()] # key is needed only to correctly handle file overwrites

    def get_pkg_files_from_workdir(self, base_name, recipe, pkgdata):
        file_dir = os.path.join(
            recipe.getVar('WORKDIR'), 'packages-split', base_name
        )
        if not os.path.isdir(file_dir):
            raise TinfoilHatException(
                f"Can't find built file dir for package '{base_name}'"
            )
        file_paths = find_files(file_dir)
        file_paths.sort()
        files = []
        file_sizes = pkgdata.get('FILES_INFO')
        for file_path in file_paths:
            realpath = file_dir + file_path
            if os.path.isdir(realpath):
                continue
            sha256 = (
                sha256sum(realpath)
                if os.path.isfile(realpath) and not os.path.islink(realpath)
                else None
            )
            files.append(BBFile(
                file_path,
                sha256,
                int(file_sizes.get(file_path) or 0)
            ))
        return file_dir, files

    def get_pkg_files(self, package_name, base_name, d):
        PKG_NAME = {
            "IPK": "%s_%s-%s_%s.ipk",
            "RPM": "%s-%s-%s.%s.rpm",
        }
        PKG_EXTRACT_CMD = {
            "IPK": "ipk=%s; data=$(ar t $ipk | grep data\.tar); ext=${data##*.}; case $ext in xz) opts=-xJf ;; zst) opts='--zstd -xf' ;; esac; ar p $ipk $data | tar $opts - --exclude='dev/*'",
            "RPM": "rpm2cpio %s | cpio -idmv"
        }
        pn = d.getVar('PN')
        pv = d.getVar('PV')
        if "package_rpm" in d.getVar("PACKAGE_CLASSES"):
            pkgclass = "RPM"
        elif "package_ipk" in d.getVar("PACKAGE_CLASSES"):
            pkgclass = "IPK"
        deploy_dir = d.getVar(f'DEPLOY_DIR_{pkgclass}')
        package_arch = d.getVar('PACKAGE_ARCH')
        if pkgclass == "RPM":
            package_arch = package_arch.replace('-', '_')
        package_arch = "noarch" if package_arch == "all" and pkgclass == "RPM" else package_arch
        subd = read_subpkgdata(base_name, d)
        package_version = subd.get("PKGV") or d.getVar("PKGV").replace('AUTOINC', '0')
        if pkgclass == "RPM":
            package_version = package_version.replace('-', '+')
        if package_version.startswith('unpinnedgit'):
            package_version = 'git'
        package_revision = subd.get("PKGR") or d.getVar("PKGR")
        if subd.get(f'PKG:{package_name}'):
            package_name = subd[f'PKG:{package_name}']
        elif subd.get(f'PKG_{package_name}'):
            package_name = subd[f'PKG_{package_name}']
        pkg_file = PKG_NAME[pkgclass] % (package_name, package_version, package_revision, package_arch)
        pkg_path = os.path.join(deploy_dir, package_arch, pkg_file)
        with tempfile.TemporaryDirectory() as unpack_tmpdir:
            bash(PKG_EXTRACT_CMD[pkgclass] % pkg_path, cwd=unpack_tmpdir)
            file_sha256s = get_dir_sha256(unpack_tmpdir)
            files = []
            for path, sha256 in file_sha256s.items():
                size = os.path.getsize(os.path.join(unpack_tmpdir, path))
                files.append(BBFile(path=path, sha256=sha256, size=size))
        return pkg_path, files


    def get_binary_package(self, package_name):
        pkgdata_dir = self.config_data.getVar('PKGDATA_DIR')
        pkgdata_file = os.path.join(pkgdata_dir, 'runtime-reverse',package_name)
        if not os.path.isfile(pkgdata_file):
            raise TinfoilHatException(f"Package '{package_name}' not found in {ps.path.dirname(pkgdata_file)}")
        try:
            with open(pkgdata_file, 'r') as f:
                pkgdata = parse_pkgdata(f)
        except:
            raise TinfoilHatException(
                f"Can't parse pkgdata file for package {package_name}"
            )
        try:
            recipe = self.tf.parse_recipe(self.mc_str + pkgdata.get('PN'))
        except:
            raise TinfoilHatException(
                f"Can't parse recipe for package '{package_name}'"
            )
        base_name = package_name
        for k, v in pkgdata.items():
            if (k.startswith('PKG_') or k.startswith('PKG:')) and v == package_name:
                base_name = k[4:]
            # it seems that there is no other way to get base package name
            # (eg. dbus' main package is 'dbus-1', but its base name is 'dbus')

        recipe_version = recipe.getVar('PV')
        package_version = pkgdata.get('PKGV') or pkgdata.get('PV') or recipe.getVar('PV')
        if package_version == 'git':
            # unpinned recipe
            srcrev = recipe.getVar('SRCREV')
            if not srcrev:
                raise TinfoilHatException(
                    f"Recipe {recipe.getVar('PN')} version is 'git' but recipe"
                    " has no SRCREV, something's wrong here"
                )
            package_version = f'unpinnedgit{srcrev[:8]}'
            if recipe_version == 'git':
                recipe_version = package_version
        revision = pkgdata.get('PR') or recipe.getVar('PR')
        package_arch = recipe.getVar('PACKAGE_ARCH')

        if self.package_classes in ['package_rpm', 'package_ipk']:
            file_dir, files = self.get_pkg_files(package_name, base_name, recipe)
        else:
            file_dir, files = self.get_pkg_files_from_workdir(
                base_name, recipe, pkgdata
            )

        return BBPackage(
                metadata = BBPackageMetadata(
                    name = package_name,
                    base_name = base_name,
                    version = package_version,
                    revision = revision,
                    package_arch = package_arch,
                    build_toolchain = self.build_toolchain,
                    recipe_name = pkgdata.get('PN'),
                    recipe_version = recipe_version,
                    recipe_revision = recipe.getVar('PR'),
                    license = (
                        pkgdata.get(f'LICENSE_{base_name}')
                        or pkgdata.get('LICENSE')
                    ),
                    summary = (
                        pkgdata.get(f'SUMMARY_{base_name}')
                        or pkgdata.get('SUMMARY')
                    ),
                    description = pkgdata.get(
                        f'DESCRIPTION_{base_name}')
                        or pkgdata.get('DESCRIPTION'
                    ),
                    depends = parse_bb_depends_provides(
                        pkgdata.get(f'RDEPENDS_{base_name}')
                        or pkgdata.get('RDEPENDS')
                    ),
                    provides = parse_bb_depends_provides(
                        pkgdata.get(f'RPROVIDES_{base_name}')
                        or pkgdata.get('RPROVIDES')
                    )
                ),
                files = BBPackageFiles(
                    file_dir = file_dir,
                    files = files
                )
        )

    def get_cve_metadata(self, recipe):
        if self.cve.get(recipe.getVar('PN')):
            cve_json = self.cve[recipe.getVar('PN')]
            with open(cve_json) as f:
                bb_cve_metadata = json.load(f)
            if len(bb_cve_metadata["package"]) > 1:
                raise TinfoilHatException(
                    "more than one package in cve metadata for recipe"
                    f" {recipe.getVar('PN')} in file {cve_json}"
                )
            cve_products = [
                CVEProduct(
                    product = p["product"],
                    cvesInRecord = p["cvesInRecord"]
                )
                for p in bb_cve_metadata["package"][0]["products"]
                # FIXME check if there may be more than one package
            ]
            cve_issue = [
                CVEIssue(
                    id = i["id"],
                    summary = i["summary"],
                    scorev2 = i["scorev2"],
                    scorev3 = i["scorev3"],
                    vector = i["vector"],
                    status = i["status"],
                    link = i["link"],
                )
                for i in bb_cve_metadata["package"][0]["issue"]
                # FIXME see above
            ]
            return CVEMetadata(products=cve_products, issue=cve_issue)
        return None

    def get_recipe(self, recipe_name, tags):
        if recipe_name not in self.recipe_cache:
            fn = self.tf.cooker.recipecaches[self.mc].pkg_pn[recipe_name][0]
            recipe_fn = re.sub(r'^[^/]*', '', fn) # remove namespace before path
            recipe = self.tf.parse_recipe(self.mc_str + recipe_name)
            self.recipe_cache[recipe_name] = (recipe_fn, recipe)
        else:
            recipe_fn, recipe = self.recipe_cache[recipe_name]
        layer_paths = recipe.getVar('BBLAYERS').split()

        found = False
        for layer_path in layer_paths:
            if recipe_fn.startswith(layer_path):
                found = True
                break
        if not found:
            raise TinfoilHatException(
                f"Can't find layer for recipe {recipe.getVar('PN')}"
            )
        layer = self.layers.get(layer_path)
        if not layer:
            raise TinfoilHatException(
                f"{recipe.getVar('PN')}: Can't find layer path {layer_path} "
                "in existing BBLAYERS, something's really wrong here!"
            )
        if recipe.getVar('PV') == 'git':
            # unpinned recipe
            srcrev = recipe.getVar('SRCREV')
            if not srcrev:
                raise TinfoilHatException(
                    f"Recipe {recipe.getVar('PN')} version is 'git' but recipe"
                    " has no SRCREV, something's wrong here"
                )
            version = f'unpinnedgit{srcrev[:8]}'
        else:
            version = recipe.getVar('PV')
        machine_tag = self.machine + (
            f"-{self.build_toolchain}" if self.build_toolchain else ""
        )
        return BBRecipe(
            metadata = BBRecipeMetadata(
                name = recipe.getVar('PN'),
                base_name = recipe.getVar('BPN'),
                version = version,
                revision = recipe.getVar('PR'),
                variant = "", # filled later on by BBRecipe.calc_chk_sum()
                build_toolchains = [ self.build_toolchain ],
                author = recipe.getVar('AUTHOR'),
                homepage = recipe.getVar('HOMEPAGE'),
                summary = recipe.getVar('SUMMARY'),
                description = recipe.getVar('DESCRIPTION'),
                license = recipe.getVar('LICENSE'),
                depends_provides = {
                    machine_tag: BBDependsProvides(
                        depends = parse_bb_depends_provides(
                            recipe.getVar('DEPENDS')
                        ),
                        provides = parse_bb_depends_provides(
                            recipe.getVar('PROVIDES')
                        )
                    )
                }
            ),
            cve_metadata = self.get_cve_metadata(recipe),
            source_files = self.get_source_files(recipe, tags, layer),
            layer = layer
        )

    def add_recipe_item(self, recipe_item: BBRecipeItem):
        m = recipe_item.recipe.metadata
        cve_m = recipe_item.recipe.cve_metadata
        recipe_id = f"{m.name}-{m.version}-{m.revision}"
        if self.bom.get(recipe_id):
            # FIXME check metadata consistency between "old" and "new" recipe
            recipe = recipe_item.recipe
            self.bom[recipe_id].check_metadata(m)
            self.bom[recipe_id].update_build_toolchains(m.build_toolchains)
            self.bom[recipe_id].update_depends_provides(m.depends_provides)
            self.bom[recipe_id].check_cve_metadata(m, cve_m)
            self.bom[recipe_id].tags.update(recipe_item.tags)
            self.bom[recipe_id].update_source_files(recipe.source_files)
        else:
            self.bom[recipe_id] = recipe_item

    def get_all_recipes(self):
        # workaround, Tinfoil.all_recipes(mc="<some_mc>") does not work
        # FIXME upstream
        self.all_recipes = {}
        for pn, fn in self.tf.cooker.recipecaches[self.mc].pkg_pn.items():
            pv = self.tf.cooker.recipecaches[self.mc].pkg_pepvpr[fn[0]][1]
            self.all_recipes.update({pn: pv})


    def process_packages(self, packages: Dict[str, List[str]]):
        for package_name, images in packages.items():
            _stderr_rewrite_line(f"parsing package '{package_name}'")
            package = self.get_binary_package(package_name)
            m = package.metadata
            recipe_id= f"{m.recipe_name}-{m.recipe_version}-{m.recipe_revision}"
            tags = BBTagPaths()
            for image in images:
                tags.add(
                    "/".join([
                        self.project,
                        self.release,
                        self.buildtag,
                        self.machine,
                        image
                    ])
                )
            _stderr_rewrite_line(f"parsing recipe '{m.recipe_name}'")
            recipe = self.get_recipe(m.recipe_name, tags.copy())
            self.add_recipe_item(BBRecipeItem(recipe, tags.copy()))
            self.bom[recipe_id].add_package_item(
                BBPackageItem(package, tags.copy())
            )
        _stderr_rewrite_line("DONE!\n")


    def process_images(self):
        self.get_all_recipes()
        self.recipe_cache = {}
        image_dir = self.config_data.getVar('DEPLOY_DIR_IMAGE')
        layer_paths = self.config_data.getVar('BBLAYERS').split()
        self.layers = {}
        for layer_path in layer_paths:
            revision = None
            remote = None
            try:
                out, _ = bash('git describe --tags --always', cwd=layer_path)
                revision = out.replace('\n','')
                out, _ = bash('git remote -v | grep "(fetch)"', cwd=layer_path)
                remote = out.split()[1]
            except:
                pass
            self.layers[layer_path] = BBLayer(layer_path, remote, revision)
        if not os.path.isdir(image_dir):
            sys.stderr.write(
                f'no images in build dir {self.build_dir}\n\n'
            )
            return
        manifests = [
            f for f in os.listdir(image_dir)
            if f.endswith('.manifest')
            and os.path.islink(os.path.join(image_dir,f))
        ]
        packages = {}
        if manifests:
            any_package_in_manifests = False
            for manifest in manifests:
                image = os.path.basename(manifest).replace(
                    f'-{self.machine}.manifest', ''
                )
                with open(os.path.join(image_dir, manifest), 'r') as f:
                    package_names = [ line.split()[0] for line in f if line ]
                    if '' in package_names:
                        package_names.remove('')
                if package_names:
                    any_package_in_manifests = True
                    for package_name in package_names:
                        if not packages.get(package_name):
                            packages[package_name] = []
                        packages[package_name].append(image)
            if any_package_in_manifests:
                self.process_packages(packages)
                return

        images = [
            f.replace('.elf', '')
            for f in os.listdir(image_dir)
            if f.endswith('.elf')
            and not os.path.islink(os.path.join(image_dir, f))
        ]
        if not images:
            sys.stderr.write(
                f'no images in build dir {self.build_dir}\n\n'
            )
            return
        elif len(images) > 1:
            sys.stderr.write(f'Multiple elf images not supported yet, skipping')
            return
        image = images[0]
        sys.stderr.write('no manifests found or no packages found in manifests,'
            ' parsing pkgdata dir\n')
        pkgdata_dir = self.config_data.getVar('PKGDATA_DIR')
        pkgdata_runtime_reverse = os.path.join(pkgdata_dir, 'runtime-reverse')
        package_names = os.listdir(pkgdata_runtime_reverse)
        packages = {package_name: [image] for package_name in package_names}
        self.process_packages(packages)

    def process(self):
        sys.stderr.write(f"Parsing build dir {self.build_dir}\n")
        for mc in self.tf.cooker.multiconfigs:
            if not self.tf.server_connection:
                # memory optimization
                self.tf.prepare()
            if len(self.tf.cooker.multiconfigs) > 1 and not mc:
                continue # skip default config, FIXME: is it right?
            self.mc = mc
            self.mc_str = f"mc:{mc}:" if mc else ""
            pn = "patch-native" # FIXME find a better way
            self.config_data = self.tf.parse_recipe(f"{self.mc_str}{pn}")
            if len(self.tf.cooker.multiconfigs) > 1:
                sys.stderr.write(f"Parsing multiconfig {self.mc}\n")
            self.distro = self.config_data.getVar('DISTRO')
            self.build_toolchain = self.config_data.getVar('TOOLCHAIN')
            self.buildtag = self.distro + (
                f"-{self.build_toolchain}" if self.build_toolchain else ""
            )
            self.machine = self.config_data.getVar('MACHINE')
            self.package_classes = self.config_data.getVar('PACKAGE_CLASSES')
            cve_check_dir = self.config_data.getVar("CVE_CHECK_DIR")
            if not cve_check_dir or not os.path.isdir(cve_check_dir):
                self.cve = {}
            else:
                self.cve = {
                    # <recipe> : <cve_file_path>
                    f.replace("_cve.json",""): os.path.join(cve_check_dir, f)
                    for f in sorted(os.listdir(cve_check_dir))
                    if not f.endswith("-native_cve.json")
                        and f.endswith("_cve.json")
                }
            self.process_images()
            self.tf.shutdown() # memory optimization

if __name__ == "__main__":

    parser = argparse.ArgumentParser(conflict_handler='resolve')

    parser.add_argument(
        "OUTPUT_DIR",
        help = "dir where to save json files with recipe metadata"
    )
    parser.add_argument(
        "PROJECT_NAME",
        help = "project name (arbitrary)"
    )
    parser.add_argument(
		"RELEASE_TAG",
		help = "project's release tag (arbitrary)"
	)
    parser.add_argument(
        "BUILD_DIR",
        nargs = "*",
        help = ("Bitbake build dir(s) to parse (wildcards allowed)."
            "Build dir(s) must have a conf/ subdir inside, with"
            " bitbake configuration files.")
    )
    try:
        args = parser.parse_args()
        build_dirs = []
        for d in args.BUILD_DIR:
            build_dirs += glob.glob(d)
        bom = BoM()
        project = args.PROJECT_NAME
        release = args.RELEASE_TAG
        if not build_dirs:
            sys.stderr.write("No BUILD_DIR provided, nothing to do here\n\n")
            sys.exit(1)
        for build_dir in build_dirs:
            with TinfoilHat(build_dir, project, release, bom) as tfh:
                tfh.process()
        os.makedirs(args.OUTPUT_DIR, exist_ok=True)
        sys.stderr.write("Cleaning tags and writing tinfoilhat files\n")
        for recipe_id, recipe_item in bom.items():
            _stderr_rewrite_line(f"cleaning and writing {recipe_id}")
            recipe_item.clean_source_file_tags()
            recipe_id_with_variant = (
                f"{recipe_id}-{recipe_item.recipe.metadata.variant}"
            )
            recipe_bom = BoM()
            recipe_bom.update({
                recipe_id_with_variant: recipe_item
            })
            fname = f'{recipe_id_with_variant}.tinfoilhat.json'
            with open(os.path.join(args.OUTPUT_DIR, fname), 'w') as f:
                f.write(recipe_bom.json)
        _stderr_rewrite_line("DONE!\n")
    except Exception as e:
        raise e
        sys.stderr.write(f"\n\nGot error {e.__class__.__name__}: {e}\n\n")
        sys.exit(1)
